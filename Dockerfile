# Alpine Node Image
FROM node:carbon-alpine

# Create app directory
WORKDIR /usr/app

# Copy package info
COPY package.json package-lock.json ./

# Install app dependencies
RUN apk add --no-cache tini git openssh && \
  npm i -g npm && \
  npm ci && \
  apk del git openssh

# Bundle app source
COPY . .

# Start Node.js
ENTRYPOINT ["/sbin/tini", "--"]
CMD [ "node", "." ]
