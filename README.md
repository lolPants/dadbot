# Dad Bot
Written by [Jack Baron](https://www.jackbaron.com)

## Public Bot
I host a public version of Dadbot. Use [this link](https://discordapp.com/oauth2/authorize?client_id=355436632834572299&permissions=117760&scope=bot) to invite it to your server.  
This doesn't have a guaranteed 100% uptime so don't cry if it's down.  
On any server it's on, type `invite dad` and it'll DM you the invite link. *(This also works on any self-hosted Dadbot)*

## Self Hosting
### Prerequisites
* Docker
* *nix based OS or Docker for Windows

### Installation
1. Pull the Docker Image: `docker pull registry.gitlab.com/lolpants/dadbot:latest`
2. Start the Docker Image: `docker run -d -e TOKEN=[YOUR DISCORD TOKEN] --name dadbot registry.gitlab.com/lolpants/dadbot:latest`  
Replace `[YOUR DISCORD TOKEN]` with your bot's token [found here.](https://discordapp.com/developers/applications/me)

### Ensuring Uptime
Add `--restart always` or `--restart on-failure` after `docker run` and the bot will restart.  
* `always` means it'll restart whenver it exits.  
* `on-failure` means it'll restart when it crashes.

Both mean the container will persist and start after a reboot.

### Stopping
Stop the container using `docker stop dadbot`  
Start it again with `docker start dadbot`

To remove the container, use `docker rm dadbot`