// Package Dependencies
const log = require('fancylog')
const exitHook = require('async-exit-hook')
const Discord = require('discord.js')
const bot = new Discord.Client()

// Environment Variables
const { TOKEN, INVIS, SON } = process.env

// Log when ready
bot.on('ready', () => {
  // Log Status
  log.i('Connected to Discord...')

  // Set status to offline
  if (INVIS !== undefined) bot.user.setStatus('invisible')
})

// On message sent
bot.on('message', async message => {
  /**
   * @type {string}
   */
  let content = message.content

  // Ignore bots
  if (message.bot) return false

  // Ignore Self
  if (message.author === bot.user) return false

  // Invite Command
  if (content.toLowerCase().includes('invite dad')) inviteCommand(message)

  // Son
  if (message.content.toLowerCase().startsWith('dadbot is the best')) {
    let sonMessage = message.author.id === SON ? 'Thank you, son.' : 'You\'re not my son!'
    return message.channel.send(sonMessage)
  }

  // Handle Dads
  /**
   * @param {Trigger} x Trigger
   * @returns {boolean}
   */
  const test = x => content.toLowerCase().startsWith(x.trigger.toLowerCase())

  const responses = {
    en: 'Hi ---, I\'m Dad.',
    fr: 'Salut ---, je suis papa.',
    de: 'Hallo ---, ich bin Papa.',
    es: 'Hola ---, soy Papá.',
  }

  /**
   * @typedef {Object} Trigger
   * @property {string} trigger
   * @property {string} lang
   * @property {string} response
   */

  /**
   * @type {Trigger[]}
   */
  const triggers = [
    // Define Triggers
    { trigger: 'im' },
    { trigger: 'i\'m' },
    { trigger: 'i am' },
    { trigger: 'you\'re' },
    { trigger: 'youre' },
    { trigger: 'you are' },
    { trigger: 'ur' },
    { trigger: 'je suis', lang: 'fr' },
    { trigger: 'ich bin', lang: 'de' },
    { trigger: 'yo soy', lang: 'es' },
  ].map(x => {
    x.trigger = `${x.trigger} `
    return x
  }).map(x => {
    // Append correct language template
    switch (x.lang) {
      case 'fr':
        x.response = responses.fr
        break
      case 'de':
        x.response = responses.de
        break
      case 'es':
        x.response = responses.es
        break
      default:
        x.response = responses.en
        break
    }
    return x
  })

  if (
    triggers.some(test)
  ) {
    // Get trigger
    let trigger = triggers.find(test)

    // Generate text to replace into
    let toReplace = content.split(' ')
      .slice(trigger.trigger.split(' ').length - 1)
      .join(' ')

    // Replace template and send
    let text = trigger.response.replace('---', toReplace)
    await message.channel.send(text)
  }
})

// Invite Command
// Ripped from catbot-2
const inviteCommand = async message => {
  // Discord Permissions
  const PERMISSIONS = [
    'VIEW_CHANNEL',
    'SEND_MESSAGES',
    'ATTACH_FILES',
    'EMBED_LINKS',
    'READ_MESSAGE_HISTORY',
  ]
  try {
    let url = await bot.generateInvite(PERMISSIONS)
    try {
      await message.author.send(`<${url}>`)
      await message.channel.send(':white_check_mark: Invite URL sent!')
    } catch (err) {
      if (err.code === 50007) message.channel.send(`<${url}>`)
    }
  } catch (err) {
    // No-op
  }
}

// Login Handler
if (!TOKEN) throw new Error('Invalid Token')
else bot.login(TOKEN)

/**
 * Logout Function
 * Handles CTRL+C and PM2
 */
exitHook(async exit => {
  // Check if logged in
  if (bot.readyAt !== null) {
    try {
      await bot.destroy()
      exit()
    } catch (err) { exit() }
  } else { exit() }
})
